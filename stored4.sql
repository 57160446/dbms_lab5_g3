DELIMITER //
DROP PROCEDURE IF EXISTS set_counter //
CREATE PROCEDURE set_counter(INOUT count INT(4), IN inc INT(4))
BEGIN
set count = count + inc;
END //
DELIMITER ;
