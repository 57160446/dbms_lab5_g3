DELIMITER //
        DROP PROCEDURE IF EXISTS CountOrderByStatus //
        CREATE PROCEDURE CountOrderByStatus(IN orderStatus VARCHAR(25),OUT total INT)
        BEGIN
                SELECT count(orderNumber)
                INTO total
                FROM orders
                WHERE status = orderStatus;
        END //
DELIMITER ;
