DELIMITER //
DROP PROCEDURE IF EXISTS GatOfficeByCountry //
CREATE PROCEDURE GetOfficeByCountry(IN countryName VARCHAR(255))
BEGIN
SELECT *
FROM offices
WHERE country = countryName;
END //
DELIMITER 
call GetOfficeByCountry('USA');
