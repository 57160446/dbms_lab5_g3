DELIMITER $$
DROP PROCEDURE IF EXISTS get_order_by_cust $$
CREATE PROCEDURE get_order_by_cust
(IN cust_no INT,
OUT shipped INT,
OUT canceled INT,
OUT resolved INT,
OUT disputed INT)
BEGIN

SELECT
count(*) INTO shipped
FROM orders
WHERE customerNumber = cust_no
AND status = 'Shipped';


SELECT
count(*) INTO canceled
FROM orders
WHERE customerNumber = cust_no
AND status = 'Cenceled';

SELECT
count(*) INTO resolved
FROM orders
WHERE customerNumber = cust_no
AND status = 'Resolved';


SELECT
count(*) INTO disputed
FROM orders
WHERE customerNumber = cust_no
AND status = 'Disputed';



END $$
DELIMITER ;
